#!/usr/bin/env bash

# Copyright (C) 2019 'icrunchbanger' icrunchbanger@gmail.com

# This software is licensed under the terms of the GNU General Public
# License version 2, as published by the Free Software Foundation, and
# may be copied, distributed, and modified under those terms.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

if [ $(grep "CentOS Linux release 7" /etc/redhat-release 2>/dev/null | wc -l) -ne 1 ]
  then echo "Not CentOS 7, aborting" ; exit 1
fi

#yum upgrade -y

wget --progress=dot https://github.com/$(wget https://github.com/prometheus/prometheus/releases/ -O - | egrep '/.*/.*/*.linux-amd64.tar.gz' -o)
mkdir -p /etc/prometheus
mkdir -p /var/lib/prometheus

useradd --no-create-home --shell /bin/false prometheus

chown -R prometheus:prometheus /etc/prometheus
chown -R prometheus:prometheus /var/lib/prometheus

tar -xvzf prometheus-*.tar.gz

cp -r prometheus-*/prometheus /usr/local/bin/
cp -r prometheus-*/promtool /usr/local/bin/
cp -r prometheus-*/consoles /etc/prometheus/
cp -r prometheus-*/console_libraries/ /etc/prometheus/

chown prometheus:prometheus /usr/local/bin/prometheus
chown prometheus:prometheus /usr/local/bin/promtool
chown -R prometheus:prometheus /etc/prometheus/consoles
chown -R prometheus:prometheus /etc/prometheus/console_libraries


cp -r prometheus-*/prometheus.yml /etc/prometheus/
chown prometheus:prometheus /etc/prometheus/prometheus.yml 

cat <<EOF | sudo tee /etc/systemd/system/prometheus.service
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl start prometheus && systemctl status prometheus

wget --progress=dot https://github.com/$(wget https://github.com/prometheus/node_exporter/releases/ -O - | egrep '/.*/.*/*.linux-amd64.tar.gz' -o)

useradd --no-create-home --shell /bin/false nodeusr
tar -xvzf node_exporter-*.tar.gz
cp -r node_exporter-*/node_exporter /usr/local/bin/
chown nodeusr:nodeusr /usr/local/bin/node_exporter

cat <<EOF | sudo tee /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=nodeusr
Group=nodeusr
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl start node_exporter && systemctl status node_exporter

cat <<EOF | sudo tee /etc/yum.repos.d/grafana.repo
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
EOF

yum install grafana -y

systemctl daemon-reload
systemctl start grafana-server && systemctl status grafana-server
